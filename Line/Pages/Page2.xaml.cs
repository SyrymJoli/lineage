﻿using Line.Models;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Pickers;

namespace Line
{
    public sealed partial class Page2 : Page
    {
        // Данные для json
        public class PersonTree
        {
            public string name { get; set; }
            public string pol { get; set; }
            public string age { get; set; }
        }
        string pol = "";
        string dayS = "";
        List<string> typsR;

        public Page2()
        {
            InitializeComponent();
            Loaded += MainPage_Loaded;

        }

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page3));
        }

        private void AppBarButton_Click_1(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page2));
            using (PersonContext db = new PersonContext())
            {
                persons.ItemsSource = db.Persons.ToList();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            panelAdd.Visibility = Visibility.Collapsed;
            Frame.Navigate(typeof(Page2));
            //очистка
            FName.Text = "";
            SNameAdd.Text = "";
            MNameAdd.Text = "";
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            smartAddPanel.Visibility = Visibility.Visible;
            cartPerson.Visibility = Visibility.Collapsed;
            TitlePanel.Text = "Добавить ";
            buttonEdit.Visibility = Visibility.Collapsed;
            buttonAdd.Visibility = Visibility.Visible;

            if (persons.SelectedItem is Person person)
            {
                using (PersonContext db = new PersonContext())
                {
                    relativeNameList.ItemsSource = db.Persons.ToList();
                    relativesList.ItemsSource = db.TypeConns.ToList();

                }
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            if (persons.SelectedItem is Person person)
            {
                using (PersonContext db = new PersonContext())
                {
                    SNameAdd.Text = person.SName;
                    FName.Text = person.FName;
                    MNameAdd.Text = person.MName;
                    if (person.Pol == "Мужской")
                    {
                        Mpol.IsChecked = true;
                    }
                    else
                    {
                        Wpol.IsChecked = true;
                    }
                    DPdr.Date.ToString(person.DayRoj);
                    if (person.DaySme == "жив")
                    {
                        toggleSwitch.IsOn = false;
                    }
                    else
                    {
                        toggleSwitch.IsOn = true;
                        DPds.Date.ToString("u" + person.DaySme);
                    }
                }
            }
            //
            cartPerson.Visibility = Visibility.Collapsed;
            panelAdd.Visibility = Visibility.Visible;
            TitlePanel.Text = "Изменить id: " + persons.SelectedIndex.ToString();
            buttonEdit.Visibility = Visibility.Visible;
            buttonAdd.Visibility = Visibility.Collapsed;
        }

        private async void RemoveButton_Click(object sender, RoutedEventArgs e)
        {
            ContentDialog deleteFileDialog = new ContentDialog()
            {
                Title = "Подтверждение действия",
                Content = "Вы действительно хотите удалить ?",
                PrimaryButtonText = "Удалить",
                SecondaryButtonText = "Отмена"
            };

            ContentDialogResult result = await deleteFileDialog.ShowAsync();

            if (result == ContentDialogResult.Primary)
            {
                //header.Text = "Файл удален";
                if (persons.SelectedItem != null)
                {
                    if (persons.SelectedItem is Person person)
                    {
                        using (PersonContext db = new PersonContext())
                        {
                            db.Persons.Remove(person);
                            db.SaveChanges();
                            persons.ItemsSource = db.Persons.ToList();
                        }
                    }
                }
            }
            else if (result == ContentDialogResult.Secondary)
            {
                //header.Text = "Отмена действия";
            }
            cartPerson.Visibility = Visibility.Collapsed;

        }
        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            using (PersonContext db = new PersonContext())
            {
                persons.ItemsSource = db.Persons.ToList();
            }
        }

        private async void buttonEdit_Click(object sender, RoutedEventArgs e)
        {
            if (SNameAdd.Text == "" || FName.Text == "" || pol == "")
            {
                ContentDialog errorDialog = new ContentDialog()
                {
                    Title = "Error: некоторые поля не золнены...",
                    Content = "где, * поля обязательно надо заполнить",
                    PrimaryButtonText = "Ok",
                    SecondaryButtonText = "Отмена"
                };
                await errorDialog.ShowAsync();
            }
            else
            {
                Person person = persons.SelectedItem as Person;
                string fullName = SNameAdd.Text + ' ' + FName.Text + ' ' + MNameAdd.Text;
                if (Mpol.IsChecked == true)
                {
                    pol = "Мужской";
                }
                else if (Wpol.IsChecked == true)
                {
                    pol = "Женский";
                }
                if (toggleSwitch.IsOn == true)
                {
                    dayS = DPds.Date.ToString("u");
                }
                else
                {
                    dayS = "жив";
                }
                using (PersonContext db = new PersonContext())
                {
                    if (person != null)
                    {
                        person.FName = FName.Text;
                        person.SName = SNameAdd.Text;
                        person.MName = MNameAdd.Text;
                        person.FullName = fullName;
                        person.DayRoj = DPdr.Date.ToString("u");
                        person.DaySme = DPds.Date.ToString("u");
                        person.Pol = pol;
                        db.Persons.Update(person);
                    }
                    else
                    {
                        db.Persons.Add(new Person { FName = FName.Text, SName = SNameAdd.Text, MName = MNameAdd.Text, DayRoj = DPdr.Date.ToString("u"), DaySme = dayS, FullName = fullName, Pol = pol });
                    }
                    db.SaveChanges();
                    persons.ItemsSource = db.Persons.ToList();
                }
                //очистка
                FName.Text = "";
                SNameAdd.Text = "";
                MNameAdd.Text = "";
                fullName = "";
            }
            cartPerson.Visibility = Visibility.Visible;
        }

        private async void buttonAdd_Click(object sender, RoutedEventArgs e)
        {
            if (SNameAdd.Text == "" && FName.Text == "" && pol == "")
            {
                ContentDialog errorDialog = new ContentDialog()
                {
                    Title = "Error: некоторые поля не золнены...",
                    Content = "где, * поля обязательно надо заполнить",
                    PrimaryButtonText = "Ok",
                    SecondaryButtonText = "Отмена"
                };
                await errorDialog.ShowAsync();
            }
            else
            {
                string fullName = SNameAdd.Text + ' ' + FName.Text + ' ' + MNameAdd.Text;
                if (Mpol.IsChecked == true)
                {
                    pol = "Мужской";
                }
                else if (Wpol.IsChecked == true)
                {
                    pol = "Женский";
                }

                if (toggleSwitch.IsOn == true)
                {
                    dayS = DPds.Date.ToString("u");
                }
                else
                {
                    dayS = "жив";
                }

                using (var db = new PersonContext())
                {
                    var nameF = new Person { FName = FName.Text, SName = SNameAdd.Text, MName = MNameAdd.Text, DayRoj = DPdr.Date.ToString("u"), DaySme = dayS, FullName = fullName, Pol = pol };
                    db.Persons.Add(nameF);
                    db.SaveChanges();
                    persons.ItemsSource = db.Persons.ToList();
                }

                //очистка
                FName.Text = "";
                SNameAdd.Text = "";
                MNameAdd.Text = "";
                fullName = "";
            }

        }

        //поиск по БД
        private void SuggestBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            using (PersonContext db = new PersonContext())
            {
                //personPoisk = new List<string> { db.Persons.ToString() };
                suggestBox.ItemsSource = db.Persons.ToList();
            }
            //suggestBox.IsSuggestionListOpen = true;
            //var term = args.QueryText.ToLower();
            //var results = suggestBox.Where(i => i.ToLower().Contains(term)).ToList();
        }

        private void suggestBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            //poisk.Text = args.SelectedItem as string;
        }

        private void ToggleSwitch_Toggled_1(object sender, RoutedEventArgs e)
        {
            if (sender is ToggleSwitch toggleSwitch)
            {
                if (toggleSwitch.IsOn == true)
                {
                    DPds.IsEnabled = true;
                }
                else
                {
                    DPds.IsEnabled = false;
                }
            }
        }

        private void persons_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Person person = persons.SelectedItem as Person;
            using (PersonContext db = new PersonContext())
            {
                if (person != null)
                {
                    cartMName.Text = person.MName;
                    cartFName.Text = person.FName;
                    cartSName.Text = person.SName;
                    cartDayRoj.Text = person.DayRoj;
                    cartDaySme.Text = person.DaySme;
                    personGrupK.Text = "";
                    personLoc.Text = "";
                    personComent.Text = "";
                    if (person.Coment != null || person.GrupK != null || person.Locachiya != null)
                    {
                        personGrupK.Text = person.GrupK;
                        personLoc.Text = person.Locachiya;
                        personComent.Text = person.Coment;
                    }
                    if (person.ImgAva != null)
                    {
                        imgPerson.Source = new BitmapImage(new Uri(person.ImgAva));
                    }
                    else if (person.ImgAva == null)
                    {
                        imgPerson.Source = new BitmapImage(new Uri("ms-appx:///Images/face_black_54x54.png"));
                    }

                    // списк родс.
                    var listRelatives = db.Relatives.ToList();
                    List<string> testList = new List<string>();
                    foreach (Relative rel in listRelatives.Where(rel => rel.Komu.ToString().StartsWith(person.PersonId.ToString())))
                    {
                        person.PersonId = rel.Kto;
                        testList.Add("IdKto: " + person.FullName + " IdTypeConn: " + rel.IdTypeConn);
                    }
                    listR.ItemsSource = testList;
                }
            }
            cartPerson.Visibility = Visibility.Visible;
            panelListR.Visibility = Visibility.Visible;
            panelTree.Visibility = Visibility.Collapsed;
            smartAddPanel.Visibility = Visibility.Collapsed;
            panelAdd.Visibility = Visibility.Collapsed;
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            // назад
            cartPerson.Visibility = Visibility.Collapsed;
            MyWebView.Visibility = Visibility.Collapsed;
            btnGD1.Visibility = Visibility.Collapsed;
            btnGD2.Visibility = Visibility.Collapsed;
            panelListR.Visibility = Visibility.Collapsed;
            panelTree.Visibility = Visibility.Collapsed;
        }

        private async void smartAddButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (relativeNameList.SelectedIndex == -1 && relativesList.SelectedIndex == -1)
                {
                    ContentDialog selectDialog = new ContentDialog()
                    {
                        Title = "Ой! не выбраны..",
                        Content = "Выберите Тип и Родственники. Если не нашли добавьте",
                        PrimaryButtonText = "Ok",
                        SecondaryButtonText = "Отмена"
                    };
                    await selectDialog.ShowAsync();
                }
                // тут
                Person person = persons.SelectedItem as Person;
                Person person2 = relativeNameList.SelectedItem as Person;
                using (PersonContext db = new PersonContext())
                {
                    if (person != null)
                    {
                        //save БД
                        var dataR = new Relative { Kto = person2.PersonId, Komu = person.PersonId, IdTypeConn = relativesList.SelectedIndex + 1 };
                        //var dataP = new Person { IdRelative = dataR.RelativeId };
                        //db.Persons.Update(dataP);
                        db.Relatives.Add(dataR);
                        db.SaveChanges();
                    }
                }
            }
            catch
            {
                //..
            }
        }   

        private void oldAdd_Click(object sender, RoutedEventArgs e)
        {
            smartAddPanel.Visibility = Visibility.Collapsed;
            panelAdd.Visibility = Visibility.Visible;
            TitlePanel.Text = "Добавить ";
            buttonEdit.Visibility = Visibility.Collapsed;
            buttonAdd.Visibility = Visibility.Visible;
        }

        private async void HyperlinkButton_Click_1(object sender, RoutedEventArgs e)
        {
            var picker = new FileOpenPicker();
            picker.ViewMode = PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".jpeg");
            picker.FileTypeFilter.Add(".png");

            StorageFolder localFolder = ApplicationData.Current.LocalFolder; //локальная папка
            // создаем папку для хранение аватаров
            StorageFolder newFolder = await localFolder.CreateFolderAsync("avas", CreationCollisionOption.ReplaceExisting);


            StorageFile file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                // скопировать аву в папу avas
                var copyFile = await file.CopyAsync(localFolder);
                await copyFile.MoveAsync(newFolder);

                // в базу надо сохронять фоту
                Person person = persons.SelectedItem as Person;
                using (PersonContext db = new PersonContext())
                {
                    if (person != null)
                    {
                        person.ImgAva = copyFile.Path;
                        db.Persons.Update(person);
                        db.SaveChanges();
                    }
                }
                imgPerson.Source = new BitmapImage(new Uri(person.ImgAva));
            }
            else
            {
                //textBlock.Text = "Operation cancelled.";
            }
        }

        private async void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            panelTree.Visibility = Visibility.Visible;
            btnGD1.Visibility = Visibility.Visible;
            btnGD2.Visibility = Visibility.Visible;
            panelListR.Visibility = Visibility.Collapsed;

            try
            {
                Person person2 = relativeNameList.SelectedItem as Person;
                Person person = persons.SelectedItem as Person;
                using (PersonContext db = new PersonContext())
                {
                    if (person != null)
                    {
                        //Данные    Кому Person
                        PersonTree mainePerson = new PersonTree
                        {
                            name = person.FullName,
                            pol = person.Pol,
                            age = person.DayRoj,
                        };
                        string pathImg = person.ImgAva;

                        //Данные    Кто Родственники
                        PersonTree relativePerson = new PersonTree
                        {
                            name = person2.FullName,
                            pol = person2.Pol,
                            age = person2.DayRoj,
                        };

                        //Десериализация
                        //string json = output;
                        //PersonTree newMusic = JsonConvert.DeserializeObject<PersonTree>(json);
                        //Сериализация
                        string sMainePerson = JsonConvert.SerializeObject(mainePerson);
                        string sPathImg = JsonConvert.SerializeObject(pathImg);

                        string sRelative = JsonConvert.SerializeObject(relativePerson);

                        string imgNewPath = sPathImg.Replace("\\+", "/");

                        //********************************
                        IEnumerable<string> begin = new string[] {
                            "let config = { container: '#basic', rootOrientation:  'SOUTH', hideRootNode: true, connectors: { type: 'step' }, node: { HTMLclass: 'nodeExample1' } }, " +
                            "person = { 'text': { 'name': '',},'image': './ava.png'}," +
                            "relative = { 'parent': person,'text': { name: 'Родственники',},'stackChildren': true,}," +
                            "parents = { 'parent': person,'text': { 'name': 'Родители',},}," +
                            "childrens = 'stackChildren': true,'parent': person,'text': { 'name': 'Дети',},},"
                        };

                        //string gdMainePerson = "ceo = { 'text': " + sMainePerson + ",'image': " + imgNewPath + "},";
                        //string gdRelative = "cto = { 'parent': ceo" + ",'text': " + sRelative + ",'image': " + imgNewPath + "},";

                        string endJson = "chart_config = [ config, person, relative, parents, childrens ]";

                        //папки в проекте
                        StorageFolder installedLocation = Windows.ApplicationModel.Package.Current.InstalledLocation;
                        //получаем папку tree
                        StorageFolder folder = await installedLocation.GetFolderAsync("tree");
                        IReadOnlyList<StorageFile> files = await folder.GetFilesAsync();
                        //локальная папка
                        StorageFolder localFolder = ApplicationData.Current.LocalFolder;
                        // создаем новыю папку Tree в локальном папке
                        StorageFolder newFolder = await localFolder.CreateFolderAsync("Tree", CreationCollisionOption.ReplaceExisting);
                        foreach (StorageFile file in files)
                        {
                            var copyFile = await file.CopyAsync(newFolder);
                        }
                        newFolder = await localFolder.GetFolderAsync("Tree");
                        // создаем файл json.js
                        StorageFile jsonFile = await newFolder.CreateFileAsync("basic.js", CreationCollisionOption.ReplaceExisting);
                        // если файл уже ранее был создан, то мы можем получить его через GetFileAsync()
                        jsonFile = await newFolder.GetFileAsync("basic.js");
                        // запись в файл
                        await FileIO.AppendLinesAsync(jsonFile, begin);
                        //await FileIO.AppendTextAsync(jsonFile, gdMainePerson);
                        //await FileIO.AppendTextAsync(jsonFile, gdRelative);
                        await FileIO.AppendTextAsync(jsonFile, endJson);
                        //********************************

                        // обновление веб-вю
                        MyWebView.Navigate(new Uri("ms-appdata:///local/Tree/treePage.html"));
                        btnGD1.Visibility = Visibility.Visible;
                        btnGD2.Visibility = Visibility.Visible;
                        panelTree.Visibility = Visibility.Visible;
                    }
                }
            }
            catch
            {
                //
            }
        }

        private void AppBarButton_Click_2(object sender, RoutedEventArgs e)
        {
            if (ListBD.Visibility == Visibility.Visible)
            {
                ListBD.Visibility = Visibility.Collapsed;
            }
            else
            {
                ListBD.Visibility = Visibility.Visible;
            }
        }

        private void AppBarButton_Click_3(object sender, RoutedEventArgs e)
        {
            panelAdd.Visibility = Visibility.Visible;
            smartAddPanel.Visibility = Visibility.Collapsed;
            cartPerson.Visibility = Visibility.Collapsed;

        }

        private void btnGD1_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnGD2_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAddDopl_Click(object sender, RoutedEventArgs e)
        {
            addDopl.Visibility = Visibility.Visible;
            cartPerson.Visibility = Visibility.Collapsed;
            Person person = persons.SelectedItem as Person;
            using (PersonContext db = new PersonContext())
            {
                if (person.Locachiya != null || person.Coment != null)
                {
                    textBoxLoc.Text = person.Locachiya;
                    textBoxComent.Text = person.Coment;
                }
                else
                {
                    textBoxLoc.Text = "";
                    textBoxComent.Text = "";
                }
            }
        }

        private void btnAddDopInfo_Click(object sender, RoutedEventArgs e)
        {
            addDopl.Visibility = Visibility.Collapsed;
            cartPerson.Visibility = Visibility.Visible;

            Person person = persons.SelectedItem as Person;
            using (PersonContext db = new PersonContext())
            {
                if (person != null)
                {
                    person.GrupK = comboBoxGupaK.SelectedValue.ToString();
                    person.Locachiya = textBoxLoc.Text;
                    person.Coment = textBoxComent.Text;
                }
                db.Persons.Update(person);
                db.SaveChanges();

                if (person.Coment != null || person.GrupK != null || person.Locachiya != null)
                {
                    personGrupK.Text = person.GrupK;
                    personLoc.Text = person.Locachiya;
                    personComent.Text = person.Coment;
                }
            }
        }

        private void btnOtmena_Click(object sender, RoutedEventArgs e)
        {
            comboBoxGupaK.SelectedIndex = -1;
            textBoxLoc.Text = "";
            textBoxComent.Text = "";
            addDopl.Visibility = Visibility.Collapsed;
            cartPerson.Visibility = Visibility.Visible;
        }

        private void drawListR_Click(object sender, RoutedEventArgs e)
        {
            panelListR.Visibility = Visibility.Visible;
            panelTree.Visibility = Visibility.Collapsed;
        }
    }
}