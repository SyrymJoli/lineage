﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Line.Migrations
{
    public partial class DBMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    PersonId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Coment = table.Column<string>(nullable: true),
                    DayRoj = table.Column<string>(nullable: true),
                    DaySme = table.Column<string>(nullable: true),
                    FName = table.Column<string>(nullable: true),
                    FullName = table.Column<string>(nullable: true),
                    GrupK = table.Column<string>(nullable: true),
                    ImgAva = table.Column<string>(nullable: true),
                    Locachiya = table.Column<string>(nullable: true),
                    MName = table.Column<string>(nullable: true),
                    Pol = table.Column<string>(nullable: true),
                    SName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.PersonId);
                });

            migrationBuilder.CreateTable(
                name: "Relatives",
                columns: table => new
                {
                    RelativeId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    IdTypeConn = table.Column<int>(nullable: false),
                    Komu = table.Column<int>(nullable: false),
                    Kto = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Relatives", x => x.RelativeId);
                });

            migrationBuilder.CreateTable(
                name: "TypeConns",
                columns: table => new
                {
                    TypeConnId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TypeConnection = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TypeConns", x => x.TypeConnId);
                });

            migrationBuilder.CreateTable(
                name: "Braks",
                columns: table => new
                {
                    BrakId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    DayRas = table.Column<string>(nullable: true),
                    DayZak = table.Column<string>(nullable: true),
                    Komu = table.Column<string>(nullable: true),
                    Kto = table.Column<string>(nullable: true),
                    PersonId = table.Column<int>(nullable: false),
                    TypeConnId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Braks", x => x.BrakId);
                    table.ForeignKey(
                        name: "FK_Braks_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "PersonId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Braks_TypeConns_TypeConnId",
                        column: x => x.TypeConnId,
                        principalTable: "TypeConns",
                        principalColumn: "TypeConnId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Braks_PersonId",
                table: "Braks",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_Braks_TypeConnId",
                table: "Braks",
                column: "TypeConnId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Braks");

            migrationBuilder.DropTable(
                name: "Relatives");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "TypeConns");
        }
    }
}
