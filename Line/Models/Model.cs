﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Line.Models
{
    public class PersonContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }
        public DbSet<Relative> Relatives { get; set; }
        public DbSet<Brak> Braks { get; set; }
        public DbSet<TypeConn> TypeConns { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=lineageDB.db");
        }
    }

    public class Person
    {
        public int PersonId { get; set; }

        public string FullName { get; set; }
        public string FName { get; set; }
        public string SName { get; set; }
        public string MName { get; set; }
        public string DayRoj { get; set; }
        public string DaySme { get; set; }
        public string Pol { get; set; }
        public string ImgAva { get; set; }

        public string GrupK { get; set; }
        public string Locachiya { get; set; }
        public string Coment { get; set; }

        //Ссылки
        //public List<Relative> Relatives { get; set; }
        //public List<Brak> Braks { get; set; }
    }

    public partial class Relative
    {
        public int RelativeId { get; set; }

        public int Kto { get; set; }
        public int Komu { get; set; }

        public int IdTypeConn { get; set; }
        // Это свойство будет использоваться как внешний ключ
        //Ссылки
        //public Person Person { get; set; }
    }

    public class Brak
    {
        public int BrakId { get; set; }

        public string Kto { get; set; }
        public string Komu { get; set; }
        public string DayZak { get; set; }
        public string DayRas { get; set; }

        [ForeignKey("PersonId")]
        public int PersonId { get; set; }

        public Person Person { get; set; }

        public TypeConn TypeConn { get; set; }
    }

    public class TypeConn
    {
        public int TypeConnId { get; set; }
        public string TypeConnection { get; set; }
    }
}
