﻿using System;
using Windows.ApplicationModel;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Media3D;
using Windows.UI.Xaml.Navigation;


namespace Line
{
    public sealed partial class Page1 : Page
    {
        private MainPage rootPage;
        public Page1()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            rootPage = MainPage.Current;
        }
        private void NormalizeParallax(UIElement target)
        {
            var transform = target.Transform3D as CompositeTransform3D;

            if (transform != null)
            {
                var transformToVisual = ParallaxRoot.TransformToVisual(target);
                var center = new Point(ParallaxRoot.ActualWidth / 2.0, RootGrid.ActualHeight / 2.0);

                // Center of ParallaxRoot in the coordinates of target.
                var transformedCenter = transformToVisual.TransformPoint(center);

                transform.CenterX = transformedCenter.X;

                // TransformToVisual doesn't account for ScrollViewer offset
                transform.CenterY = transformedCenter.Y - ParallaxRoot.VerticalOffset;

                // This could be done statically in markup but it's easier to show here.
                transform.ScaleX = transform.ScaleY =
                    -transform.TranslateZ / PerspectiveTransform.Depth + 1.0;
            }
        }

        private void RootGrid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            NormalizeParallax(RainierBackgroundImage);
            NormalizeParallax(CliffBackgroundImage);
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage));
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility =
        AppViewBackButtonVisibility.Visible;
        }
        private void App_BackRequested(object sender, Windows.UI.Core.BackRequestedEventArgs e)
        {
            Frame frame = Window.Current.Content as Frame;
            if (frame.CanGoBack)
            {
                frame.GoBack(); // переход назад
                e.Handled = true; // указываем, что событие обработано
            }
        }
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            deferral.Complete();
        }
        

        private void HyperlinkButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(Page2));
        }
    }
}
