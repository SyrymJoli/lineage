﻿using System;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;


namespace Line
{
    public sealed partial class PageSettings : Page
    {
        public PageSettings()
        {
            InitializeComponent();
        }

        private void Ssdf(object sender, ItemClickEventArgs e)
        {
            PassW.Visibility = Windows.UI.Xaml.Visibility.Visible;
            ListMenu.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        }

        private void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            PassW.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            ListMenu.Visibility = Windows.UI.Xaml.Visibility.Visible;
        }

        private async void Button_Click_1(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile passWFile = await storageFolder.CreateFileAsync("passSyrym", CreationCollisionOption.OpenIfExists);

            string passNow = await FileIO.ReadTextAsync(passWFile);//Чтение passWord из файла

            if (passNowE.Password == passNow)
            {
                if (passFuture1.Password == passFuture2.Password)
                {
                    string newPassNow = passFuture1.Password;
                    await FileIO.WriteTextAsync(passWFile, newPassNow);// Запись текста в файл

                    MessageDialog dialog = new MessageDialog("Пароль успешно изменен ");
                    dialog.Title = "Успешно! ";
                    await dialog.ShowAsync();

                    PassW.Visibility = Windows.UI.Xaml.Visibility.Collapsed;// тут говноКод
                    ListMenu.Visibility = Windows.UI.Xaml.Visibility.Visible;
                }
                else
                {
                    MessageDialog dialog = new MessageDialog("ой! пароли не совпадают.. ");
                    dialog.Title = "Ошибка! ";
                    await dialog.ShowAsync();
                }
            }

        }
    }
}
