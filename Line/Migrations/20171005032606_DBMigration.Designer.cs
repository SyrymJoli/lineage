﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Line.Models;

namespace Line.Migrations
{
    [DbContext(typeof(PersonContext))]
    [Migration("20171005032606_DBMigration")]
    partial class DBMigration
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.1");

            modelBuilder.Entity("Line.Models.Brak", b =>
                {
                    b.Property<int>("BrakId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("DayRas");

                    b.Property<string>("DayZak");

                    b.Property<string>("Komu");

                    b.Property<string>("Kto");

                    b.Property<int>("PersonId");

                    b.Property<int?>("TypeConnId");

                    b.HasKey("BrakId");

                    b.HasIndex("PersonId");

                    b.HasIndex("TypeConnId");

                    b.ToTable("Braks");
                });

            modelBuilder.Entity("Line.Models.Person", b =>
                {
                    b.Property<int>("PersonId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Coment");

                    b.Property<string>("DayRoj");

                    b.Property<string>("DaySme");

                    b.Property<string>("FName");

                    b.Property<string>("FullName");

                    b.Property<string>("GrupK");

                    b.Property<string>("ImgAva");

                    b.Property<string>("Locachiya");

                    b.Property<string>("MName");

                    b.Property<string>("Pol");

                    b.Property<string>("SName");

                    b.HasKey("PersonId");

                    b.ToTable("Persons");
                });

            modelBuilder.Entity("Line.Models.Relative", b =>
                {
                    b.Property<int>("RelativeId")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("IdTypeConn");

                    b.Property<int>("Komu");

                    b.Property<int>("Kto");

                    b.HasKey("RelativeId");

                    b.ToTable("Relatives");
                });

            modelBuilder.Entity("Line.Models.TypeConn", b =>
                {
                    b.Property<int>("TypeConnId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("TypeConnection");

                    b.HasKey("TypeConnId");

                    b.ToTable("TypeConns");
                });

            modelBuilder.Entity("Line.Models.Brak", b =>
                {
                    b.HasOne("Line.Models.Person", "Person")
                        .WithMany()
                        .HasForeignKey("PersonId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Line.Models.TypeConn", "TypeConn")
                        .WithMany()
                        .HasForeignKey("TypeConnId");
                });
        }
    }
}
