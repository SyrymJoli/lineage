﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Line
{
    class PageInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public sealed partial class MainPage : Page
    {
        public static MainPage Current;
        public MainPage()
        {
            this.InitializeComponent();
            Current = this;
        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MyFrame.Navigate(typeof(Page1));
        }

        private void AboutButton_Click(object sender, RoutedEventArgs e)
        {
            MyFrame.Navigate(typeof(Page2));
        }

        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (MyFrame.CanGoBack)
            {
                MyFrame.GoBack();
            }
        }

        private void ForwardButton_Click(object sender, RoutedEventArgs e)
        {
            if (MyFrame.CanGoForward)
            {
                MyFrame.GoForward();
            }
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            MySplitView.IsPaneOpen = !MySplitView.IsPaneOpen;
        }

        private void IconsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (AboutListBoxItem.IsSelected) { MyFrame.Navigate(typeof(PageAbout)); }
            if (OListBoxItem.IsSelected) { MyFrame.Navigate(typeof(PageReviewsApp)); }
            if (SettingsListBoxItem.IsSelected) { MyFrame.Navigate(typeof(PageSettings)); }
            if (HomeListBoxItem.IsSelected) { MyFrame.Navigate(typeof(Page1)); }
            if (HellpListBoxItem.IsSelected) { MyFrame.Navigate(typeof(Page3)); }
            else if (FavoritesListBoxItem.IsSelected) { MyFrame.Navigate(typeof(Page2)); }
        }
    }
}
