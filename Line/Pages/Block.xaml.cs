﻿using System;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace Line
{
    public sealed partial class Block : Page
    {
        public Block()
        {
            this.InitializeComponent();
        }

        private async void Next_Click(object sender, RoutedEventArgs e)
        {
            //string passSyrym ="12345";
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile passWFile = await storageFolder.CreateFileAsync("passSyrym", CreationCollisionOption.OpenIfExists);

            string passNow = await FileIO.ReadTextAsync(passWFile);//Чтение passWord из файла

            if (passW.Password == passNow)
            {
                this.Frame.Navigate(typeof(MainPage));
            }
            else
            {
                smsPass.Text = "Ой! пароль неверно попробуй еще раз";
                progressRing.IsActive = false;
            }
            progressRing.IsActive = true;
            
        }
    }
}
