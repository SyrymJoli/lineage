let config = {
    container: '#basic',
    rootOrientation:  'SOUTH', // NORTH || EAST || WEST || SOUTH
    hideRootNode: true,
    
    connectors: {
        type: 'step' //step curv
    },
    node: {
        HTMLclass: 'nodeExample1'
    }
},
    person = {'text': {'name': 'Персона',},'image': './ava.png'},

    relative = {"parent": person,"text": {name: "Родственники",},"stackChildren": true,},

	parents = {"parent": person,"text": {"name": "Родители",},},

    childrens = {"stackChildren": true,"parent": person,"text": {"name": "Дети",},},

	r1 = {
        "parent": relative,
        "text": {
            name: "..",
        },
        "stackChildren": true,
    },
    
    children1 = {
        "parent": childrens,
        "text": {
            name: "Сын",
        },
        "image": "./ava.png"
    },

    children2 = {
        "parent": childrens,
        "text": {
            name: "Дочь",
        },
        "image": "./ava.png"
    },
    
    parent1 = {
        "parent": parents,
        "text": {
            "name": "Отец",
        },
        "image": "./ava.png"
    },

    parent2 = {
        "parent": parents,
        "text": {
            "name": "Мать",
        },
        "image": "./ava.png"
    },


chart_config = [
    config,
	    person,
	    relative,
	    parents,
		childrens,
	    
	    r1,
	    parent1,
	    parent2,
	    children1,
	    children2,
];
